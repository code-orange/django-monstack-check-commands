from django.db import models


class AbstractCheckCommand(models.Model):
    computer_id_ici = models.IntegerField(blank=False, null=False)
    check_enabled = models.BooleanField(default=True)
    notify_enabled = models.BooleanField(default=True)
    run_on_endpoint = models.BooleanField(default=True)
    check_interval = models.IntegerField(default=30)
    check_timeout = models.IntegerField(default=30)
    check_retry_interval = models.IntegerField(default=30)
    check_attempts = models.IntegerField(default=3)

    class Meta:
        abstract = True


class CheckModbus(AbstractCheckCommand):
    address = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="The Host IP Address",
        verbose_name="-i",
        default="127.0.0.1",
    )
    modbus_port = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="Host Port MODBUS",
        verbose_name="-p",
        default="502",
    )
    modbus_read_coil = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="Address read Coil",
        verbose_name="-r",
        default="0",
    )
    modbus_good_state = models.CharField(
        max_length=256,
        null=False,
        blank=False,
        help_text="Good State of Check 0 or 1",
        verbose_name="-w",
        default="0",
    )

    def mon_core_check_name(self):
        matrix = {
            "icinga2": "modbus",
        }
        return matrix

    def mon_core_check_command(self):
        return "check_modbus"

    class Meta:
        db_table = "check_modbus"
