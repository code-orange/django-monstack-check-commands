# Generated by Django 3.0.6 on 2020-06-03 12:28

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("django_monstack_check_commands", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="checkmodbus",
            name="address",
            field=models.CharField(
                default="127.0.0.1",
                help_text="The Host IP Address",
                max_length=256,
                verbose_name="-i",
            ),
        ),
        migrations.AlterField(
            model_name="checkmodbus",
            name="modbus_good_state",
            field=models.CharField(
                default="0",
                help_text="Good State of Check 0 or 1",
                max_length=256,
                verbose_name="-w",
            ),
        ),
        migrations.AlterField(
            model_name="checkmodbus",
            name="modbus_port",
            field=models.CharField(
                default="502",
                help_text="Host Port MODBUS",
                max_length=256,
                verbose_name="-p",
            ),
        ),
        migrations.AlterField(
            model_name="checkmodbus",
            name="modbus_read_coil",
            field=models.CharField(
                default="0",
                help_text="Address read Coil",
                max_length=256,
                verbose_name="-r",
            ),
        ),
    ]
